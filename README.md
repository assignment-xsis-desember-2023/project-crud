# Project CRUD

Repositori ini dibuat sebagai bagian dari pengerjaan tugas Project CRUD yang diberikan selama pre-placement Xsis Mitra Utama. Ia mengandung kode aplikasi Android untuk proyek CRUD nomor 1, yaitu Perpustakaan.

## Instalasi
* Android Studio
  1. Clone repositori ini menggunakan Android Studio
  2. Jalankan aplikasi di Android Studio
* APK
  1. Unduh APK dari direktori [release](release)
  2. Instal APK

### Dependensi
* Versi Android minimal: Android 8

## Perpustakaan

### Fitur yang tersedia
* Melihat daftar buku yang ada di basis data
* Masuk sebagai satu di antara empat pengguna yang tersedia di basis data
* Mengajukan peminjaman buku
* Mengganti selang waktu peminjaman
* Menandai buku sebagai favorit

#### Fitur admin
* Menyetujui permintaan peminjaman
* Menyetujui penggantian selang waktu peminjaman
* Mengonfirmasi jika buku sudah dikembalikan

### Cara penggunaan
* Saat aplikasi dibuka, terdapat menu dropdown dan tombol "Masuk"
  * Dropdown berisi daftar pengguna dan digunakan untuk memilih masuk sebagai siapa
  * Tombol hanya bisa ditekan jika pengguna sudah dipilih
* Setelah masuk, pengguna masuk ke halaman hub yang terdiri dari tombol ke Daftar Buku, tombol ke Daftar Pinjaman, dan tombol ke Pengelolaan Pinjaman (khusus admin)

#### Daftar Buku
* Semua buku ditampilkan dalam daftar vertikal
* Buku yang sedang dipinjam atau diajukan peminjamannya tidak bisa dipinjam
* Buku yang tersedia memiliki tombol "Pinjam" yang jika ditekan akan membawa pengguna ke halaman Peminjaman Buku
* Setiap buku memiliki tombol ikon hati yang menandakan apakah buku tersebut adalah favorit atau bukan
  * Jika tombol ditekan, maka status favoritnya akan berganti

#### Daftar Buku Favorit
* Semua buku favorit pengguna ditampilkan dalam daftar vertikal
* Setiap buku memiliki tombol "Hapus dari Favorit" yang akan langsung menghapus buku tersebut dari daftar buku favorit begitu ditekan
* Setiap buku yang tersedia memiliki tombol "Pinjam" yang jika ditekan akan membawa pengguna ke halaman Peminjaman Buku

#### Daftar Pinjaman
* Semua peminjaman yang pernah diajukan ditampilkan dalam daftar vertikal dan dikelompokkan berdasarkan status pengajuan (approved, unapproved, rejected)
* Peminjaman yang sudah disetujui bisa dibedakan menjadi dua: yang sudah dikembalikan dan yang belum dikembalikan
  * Untuk mengembalikan buku, harus melewati pengguna admin
  * Buku yang belum dikembalikan punya tombol "Request Extension" untuk mengganti tanggal batas pengembalian
    * Jika tombol ditekan, pengguna akan dibawa ke halaman Peminjaman Buku
* Peminjaman yang belum disetujui punya tombol "Edit Request" untuk mengganti tanggal mulai dan batas akhir peminjaman
  * Untuk membuat pengajuan disetujui, harus melewati pengguna admin
* Peminjaman yang sudah ditolak hanya ditampilkan saja
* Setiap buku memiliki tombol ikon hati yang menandakan apakah buku tersebut adalah favorit atau bukan
  * Jika tombol ditekan, maka status favoritnya akan berganti

#### Peminjaman Buku
Halaman untuk mengubah detail pengajuan pinjaman buku
* Terdapat dua kotak untuk masing-masing input tanggal dengan nilai default:
  * Tanggal pinjam: hari ini
  * Tanggal pengembalian: tujuh hari setelah hari ini
* Setiap kotak bisa ditekan untuk menampilkan dialog pemilih tanggal
  * Kotak pemilih tanggal mulai meminjam hanya bisa ditekan jika status pengajuan peminjamannya belum disetujui
* Dialog pemilih tanggal punya batas tanggal terawal yang bisa dipilih
  * Tanggal pinjam: hari ini
  * Tanggal pengembalian: yang lebih akhir di antara hari ini dan tanggal pinjam

#### Pengelolaan Pinjaman
* Semua peminjaman yang masih aktif ditampilkan dalam daftar vertikal dan dikelompokkan menjadi tiga: belum dikembalikan, meminta penggantian tanggal batas pengembalian, dan belum disetujui
  * Peminjaman disebut aktif jika belum ditolak atau belum dikembalikan
* Peminjaman yang belum dikembalikan dan tanggal peminjamannya adalah hari ini atau sebelumnya punya tombol "Confirm Return"
  * Saat tombol tersebut ditekan, admin menandai buku telah dikembalikan dan proses peminjaman sudah selesai
* Peminjaman yang meminta penggantian tanggal batas pengembalian punya dua tombol, yaitu "Approve" dan "Reject"
  * Keduanya menghapus permohonan penggantian batas pengembalian jika ditekan
  * Tombol "Approve" juga mengganti batas pengembalian yang lama dengan yang baru jika ditekan
* Peminjaman yang belum disetujui punya dua tombol:
  * "Approve" mengganti status peminjaman menjadi approved/disetujui
  * "Reject" mengganti status peminjaman menjadi rejected/ditolak

### Status proyek
Tidak ada pembaruan yang direncanakan

<!-- Styling for second-level numbered list items -->
<style type="text/css">
    li ol { list-style-type: decimal; }
</style>