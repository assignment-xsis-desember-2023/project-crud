package id.bootcamp.perpustakaan.data.repository

import id.bootcamp.perpustakaan.data.room.dao.UserDao
import id.bootcamp.perpustakaan.data.room.entity.UserEntity

class UserRepository private constructor(
    private val userDao: UserDao
) {

    suspend fun getAllUsers(): List<UserEntity> {
        return userDao.getAllUsers()
    }

    suspend fun getUserById(id: Int): UserEntity {
        return userDao.getUserById(id)
    }

    companion object {
        @Volatile
        private var instance: UserRepository? = null
        fun getInstance(
            userDao: UserDao
        ): UserRepository = instance ?: synchronized(this) {
            instance ?: UserRepository(userDao)
        }
    }
}