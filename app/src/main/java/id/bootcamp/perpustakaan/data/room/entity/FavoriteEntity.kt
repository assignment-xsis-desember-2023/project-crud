package id.bootcamp.perpustakaan.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "favorite",
    primaryKeys = ["book_id", "user_id"],
    foreignKeys = [
        ForeignKey(
            entity = BookEntity::class,
            parentColumns = ["id"],
            childColumns = ["book_id"],
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = ["id"],
            childColumns = ["user_id"],
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
class FavoriteEntity (
    @ColumnInfo("book_id")
    val bookId: Int,

    @ColumnInfo("user_id")
    val userId: Int
)