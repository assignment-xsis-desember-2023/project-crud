package id.bootcamp.perpustakaan.data.room.entity.relation

import androidx.room.Embedded
import androidx.room.Relation
import id.bootcamp.perpustakaan.data.room.entity.BookEntity
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.data.room.entity.UserEntity

data class BorrowingWithBookAndUser(
    @Embedded
    val borrowing: BorrowingEntity,

    @Relation(
        entity = BookEntity::class,
        entityColumn = "id",
        parentColumn = "book_id"
    )
    val book: BookEntity,

    @Relation(
        entity = UserEntity::class,
        entityColumn = "id",
        parentColumn = "user_id"
    )
    val user: UserEntity
)
