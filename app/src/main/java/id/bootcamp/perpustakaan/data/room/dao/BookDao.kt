package id.bootcamp.perpustakaan.data.room.dao

import androidx.room.Dao
import androidx.room.Query
import id.bootcamp.perpustakaan.data.room.entity.BookEntity

@Dao
interface BookDao {
    @Query("""
        SELECT * FROM book
    """)
    suspend fun getAllBooks(): List<BookEntity>

    @Query("""
        SELECT * FROM book WHERE id = :id
    """)
    suspend fun getBookById(id: Int): BookEntity
}