package id.bootcamp.perpustakaan.data.room.typeconverter

import androidx.room.TypeConverter
import id.bootcamp.perpustakaan.utils.ApprovalStatus

class ApprovalStatusConverter {
    @TypeConverter
    fun toStatus(string: String): ApprovalStatus {
        return when (string) {
            "approved" -> ApprovalStatus.APPROVED
            "rejected" -> ApprovalStatus.REJECTED
            else -> ApprovalStatus.UNAPPROVED
        }
    }

    @TypeConverter
    fun statusToString(status: ApprovalStatus): String {
        return when (status) {
            ApprovalStatus.APPROVED -> "approved"
            ApprovalStatus.REJECTED -> "rejected"
            ApprovalStatus.UNAPPROVED -> "unapproved"
        }
    }
}