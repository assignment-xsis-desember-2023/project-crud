package id.bootcamp.perpustakaan.data.room.dao

import androidx.room.Dao
import androidx.room.Query
import id.bootcamp.perpustakaan.data.room.entity.UserEntity

@Dao
interface UserDao {
    @Query("""
        SELECT * FROM user
    """)
    suspend fun getAllUsers(): List<UserEntity>

    @Query("""
        SELECT * FROM user WHERE id = :id
    """)
    suspend fun getUserById(id: Int): UserEntity
}