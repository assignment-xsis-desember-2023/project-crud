package id.bootcamp.perpustakaan.data.repository

import id.bootcamp.perpustakaan.data.room.dao.BorrowingDao
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.data.room.entity.relation.BorrowingWithBookAndUser
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import java.util.Date

class BorrowingRepository private constructor(
    private val borrowingDao: BorrowingDao
) {
    suspend fun getAllBorrowings(): List<BorrowingWithBookAndUser> {
        return borrowingDao.getAllBorrowings()
    }

    suspend fun getBorrowingById(id: Int): BorrowingEntity {
        return borrowingDao.getBorrowingById(id)
    }

    suspend fun getBorrowingsByUser(userId: Int): List<BorrowingWithBookAndUser> {
        return borrowingDao.getBorrowingListByUser(userId)
    }

    suspend fun addNewBorrowing(borrowing: BorrowingEntity) {
        borrowingDao.addNewBorrowing(borrowing)
    }

    suspend fun updateBorrowing(borrowing: BorrowingEntity) {
        borrowingDao.updateBorrowing(borrowing)
    }

    suspend fun updateBorrowingApprovalStatus(
        borrowing: BorrowingWithBookAndUser,
        status: ApprovalStatus
    ) {
        val updatedBorrowing = borrowing.borrowing
        updatedBorrowing.approvalStatus = status
        borrowingDao.updateBorrowing(updatedBorrowing)
    }

    suspend fun updateBorrowingDeadlineExtension(
        borrowing: BorrowingWithBookAndUser,
        changeDeadline: Boolean
    ) {
        val requestedDeadline = borrowing.borrowing.returnDeadlineExtension
        val updatedBorrowing = borrowing.borrowing
        updatedBorrowing.returnDeadlineExtension = null
        if (changeDeadline && requestedDeadline != null) {
            updatedBorrowing.returnDeadline = requestedDeadline
        }
        borrowingDao.updateBorrowing(updatedBorrowing)
    }

    suspend fun updateBorrowingReturnDate(
        borrowing: BorrowingWithBookAndUser,
        returnDate: Date = Date()
    ) {
        val updatedBorrowing = borrowing.borrowing
        updatedBorrowing.returnDate = returnDate
        borrowingDao.updateBorrowing(updatedBorrowing)
    }

    companion object {
        @Volatile
        private var instance: BorrowingRepository? = null
        fun getInstance(
            borrowingDao: BorrowingDao
        ): BorrowingRepository = instance ?: synchronized(this) {
            instance ?: BorrowingRepository(borrowingDao)
        }
    }
}