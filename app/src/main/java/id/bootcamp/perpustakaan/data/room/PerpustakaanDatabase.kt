package id.bootcamp.perpustakaan.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.perpustakaan.data.room.dao.BookDao
import id.bootcamp.perpustakaan.data.room.dao.BorrowingDao
import id.bootcamp.perpustakaan.data.room.dao.FavoriteDao
import id.bootcamp.perpustakaan.data.room.dao.UserDao
import id.bootcamp.perpustakaan.data.room.entity.BookEntity
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.data.room.entity.FavoriteEntity
import id.bootcamp.perpustakaan.data.room.entity.UserEntity
import id.bootcamp.perpustakaan.data.room.typeconverter.ApprovalStatusConverter
import id.bootcamp.perpustakaan.data.room.typeconverter.DateConverter

@Database(
    entities = [
        BookEntity::class,
        UserEntity::class,
        BorrowingEntity::class,
        FavoriteEntity::class
    ],
    version = 5
)
@TypeConverters(DateConverter::class, ApprovalStatusConverter::class)
abstract class PerpustakaanDatabase: RoomDatabase() {

    abstract fun bookDao(): BookDao

    abstract fun borrowingDao(): BorrowingDao

    abstract fun userDao(): UserDao

    abstract fun favoriteDao(): FavoriteDao

    companion object {
        @Volatile
        private var instance: PerpustakaanDatabase? = null
        fun getInstance(context: Context): PerpustakaanDatabase = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(
                context.applicationContext,
                PerpustakaanDatabase::class.java,
                "perpustakaan.db"
            ).createFromAsset("database/perpustakaan.db")
                .addMigrations(migrate1to2, migrate2to3, migrate3to4, migrate4to5)
                .build()
        }

        private val migrate1to2 = object: Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("""
                    CREATE TABLE "user" (
                    	"id"	INTEGER NOT NULL UNIQUE,
                    	"name"	TEXT NOT NULL,
                    	"user_group"	TEXT NOT NULL,
                    	PRIMARY KEY("id" AUTOINCREMENT)
                    )
                """)
                db.execSQL("""
                    CREATE TABLE "borrowings" (
                    	"id"	INTEGER NOT NULL UNIQUE,
                    	"book_id"	INTEGER NOT NULL,
                    	"user_id"	INTEGER NOT NULL,
                    	"borrow_date"	INTEGER NOT NULL,
                    	"return_deadline"	INTEGER NOT NULL,
                    	"return_date"	INTEGER,
                    	FOREIGN KEY("book_id") REFERENCES "book"("id") ON UPDATE CASCADE,
                    	FOREIGN KEY("user_id") REFERENCES "user"("id") ON UPDATE CASCADE,
                    	PRIMARY KEY("id" AUTOINCREMENT)
                    )
                """)
            }
        }

        private val migrate2to3 = object: Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("""
                    ALTER TABLE "borrowings"
                    ADD "approval_status" TEXT NOT NULL DEFAULT 'unapproved'
                """.trimIndent())
            }
        }

        private val migrate3to4 = object: Migration(3, 4) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("""
                    ALTER TABLE "borrowings"
                    ADD "return_deadline_extension" INTEGER
                """)
            }
        }

        private val migrate4to5 = object: Migration(4, 5) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("""
                    CREATE TABLE "favorite" (
	                    "book_id"	INTEGER NOT NULL,
	                    "user_id"	INTEGER NOT NULL,
	                    FOREIGN KEY("book_id") REFERENCES "book"("id") ON UPDATE CASCADE,
	                    FOREIGN KEY("user_id") REFERENCES "user"("id") ON UPDATE CASCADE,
	                    PRIMARY KEY("book_id","user_id")
                    )
                """)
            }
        }
    }
}