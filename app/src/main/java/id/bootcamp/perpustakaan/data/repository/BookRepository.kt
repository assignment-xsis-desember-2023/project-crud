package id.bootcamp.perpustakaan.data.repository

import id.bootcamp.perpustakaan.data.room.dao.BookDao
import id.bootcamp.perpustakaan.data.room.entity.BookEntity

class BookRepository private constructor(
    private val bookDao: BookDao
) {

    suspend fun getAllBooks(): List<BookEntity> {
        return bookDao.getAllBooks()
    }

    suspend fun getBookById(id: Int): BookEntity {
        return bookDao.getBookById(id)
    }

    companion object {
        @Volatile
        private var instance: BookRepository? = null
        fun getInstance(
            bookDao: BookDao
        ): BookRepository = instance ?: synchronized(this) {
            instance ?: BookRepository(bookDao)
        }
    }
}