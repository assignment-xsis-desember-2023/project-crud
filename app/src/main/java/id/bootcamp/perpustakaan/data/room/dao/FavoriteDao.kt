package id.bootcamp.perpustakaan.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import id.bootcamp.perpustakaan.data.room.entity.FavoriteEntity

@Dao
interface FavoriteDao {
    @Query("""
        SELECT EXISTS (SELECT * FROM favorite WHERE book_id = :bookId AND user_id = :userId)
    """)
    suspend fun find(bookId: Int, userId: Int): Boolean

    @Insert
    suspend fun add(favoriteEntity: FavoriteEntity)

    @Delete
    suspend fun delete(favoriteEntity: FavoriteEntity)
}