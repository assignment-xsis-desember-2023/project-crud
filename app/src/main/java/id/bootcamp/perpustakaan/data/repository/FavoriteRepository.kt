package id.bootcamp.perpustakaan.data.repository

import id.bootcamp.perpustakaan.data.room.dao.FavoriteDao
import id.bootcamp.perpustakaan.data.room.entity.FavoriteEntity

class FavoriteRepository private constructor(
    private val favoriteDao: FavoriteDao
) {

    suspend fun isLiked(bookId: Int, userId: Int): Boolean {
        return favoriteDao.find(bookId, userId)
    }

    suspend fun findEntity(favoriteEntity: FavoriteEntity): Boolean {
        return favoriteDao.find(favoriteEntity.bookId, favoriteEntity.userId)
    }

    suspend fun addFavorite(addedFavorite: FavoriteEntity) {
        return favoriteDao.add(addedFavorite)
    }

    suspend fun deleteFavorite(deletedFavorite: FavoriteEntity) {
        return favoriteDao.delete(deletedFavorite)
    }

    companion object {
        @Volatile
        private var instance: FavoriteRepository? = null
        fun getInstance(
            favoriteDao: FavoriteDao
        ): FavoriteRepository = instance ?: synchronized(this) {
            instance ?: FavoriteRepository(favoriteDao)
        }
    }
}