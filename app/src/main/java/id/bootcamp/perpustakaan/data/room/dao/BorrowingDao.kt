package id.bootcamp.perpustakaan.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.data.room.entity.relation.BorrowingWithBookAndUser

@Dao
interface BorrowingDao {
    @Query("""
        SELECT * FROM borrowings
    """)
    suspend fun getAllBorrowings(): List<BorrowingWithBookAndUser>

    @Query("""
        SELECT * FROM borrowings WHERE id = :id
    """)
    suspend fun getBorrowingById(id: Int): BorrowingEntity

    @Query("""
        SELECT * FROM borrowings WHERE user_id = :userId
    """)
    suspend fun getBorrowingListByUser(userId: Int): List<BorrowingWithBookAndUser>

    @Insert
    suspend fun addNewBorrowing(borrowing: BorrowingEntity)

    @Update
    suspend fun updateBorrowing(borrowing: BorrowingEntity)
}