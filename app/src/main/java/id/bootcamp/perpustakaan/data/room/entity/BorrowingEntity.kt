package id.bootcamp.perpustakaan.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import java.util.Date

@Entity(
    tableName = "borrowings",
    foreignKeys = [
        ForeignKey(
            entity = BookEntity::class,
            parentColumns = ["id"],
            childColumns = ["book_id"],
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = ["id"],
            childColumns = ["user_id"],
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class BorrowingEntity(
    @ColumnInfo("id")
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,

    @ColumnInfo("book_id")
    val bookId: Int,

    @ColumnInfo("user_id")
    val userId: Int,

    @ColumnInfo("borrow_date")
    val borrowDate: Date,

    @ColumnInfo("return_deadline")
    var returnDeadline: Date,

    @ColumnInfo("return_date")
    var returnDate: Date? = null,

    @ColumnInfo("approval_status")
    var approvalStatus: ApprovalStatus = ApprovalStatus.UNAPPROVED,

    @ColumnInfo("return_deadline_extension")
    var returnDeadlineExtension: Date? = null
)
