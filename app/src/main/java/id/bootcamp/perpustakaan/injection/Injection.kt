package id.bootcamp.perpustakaan.injection

import android.content.Context
import id.bootcamp.perpustakaan.data.repository.BookRepository
import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.repository.FavoriteRepository
import id.bootcamp.perpustakaan.data.repository.UserRepository
import id.bootcamp.perpustakaan.data.room.PerpustakaanDatabase
import id.bootcamp.perpustakaan.domain.usecase.BorrowBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBookListUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBorrowingListByUserUseCase
import id.bootcamp.perpustakaan.domain.usecase.RequestDeadlineExtensionUseCase
import id.bootcamp.perpustakaan.domain.usecase.ToggleLikeBookUseCase

object Injection {
    private fun provideBookRepository(context: Context): BookRepository {
        return BookRepository.getInstance(PerpustakaanDatabase.getInstance(context).bookDao())
    }

    fun provideBorrowingRepository(context: Context): BorrowingRepository {
        return BorrowingRepository.getInstance(PerpustakaanDatabase.getInstance(context).borrowingDao())
    }

    private fun provideFavoriteRepository(context: Context): FavoriteRepository {
        return FavoriteRepository.getInstance(PerpustakaanDatabase.getInstance(context).favoriteDao())
    }

    fun provideUserRepository(context: Context): UserRepository {
        return UserRepository.getInstance(PerpustakaanDatabase.getInstance(context).userDao())
    }

    fun provideGetBookListUseCase(context: Context): GetBookListUseCase {
        return GetBookListUseCase(
            provideBookRepository(context),
            provideBorrowingRepository(context),
            provideFavoriteRepository(context)
        )
    }

    fun provideBorrowBookUseCase(context: Context): BorrowBookUseCase {
        return BorrowBookUseCase(
            provideBorrowingRepository(context)
        )
    }

    fun provideGetBookUseCase(context: Context): GetBookUseCase {
        return GetBookUseCase(
            provideBookRepository(context),
            provideFavoriteRepository(context)
        )
    }

    fun provideRequestDeadlineExtensionUseCase(context: Context): RequestDeadlineExtensionUseCase {
        return RequestDeadlineExtensionUseCase(
            provideBorrowingRepository(context)
        )
    }

    fun provideToggleLikeBookUseCase(context: Context): ToggleLikeBookUseCase {
        return ToggleLikeBookUseCase(
            provideFavoriteRepository(context)
        )
    }

    fun provideGetBorrowingListByUserUseCase(context: Context): GetBorrowingListByUserUseCase {
        return GetBorrowingListByUserUseCase(
            provideBorrowingRepository(context),
            provideFavoriteRepository(context)
        )
    }
}