package id.bootcamp.perpustakaan.utils

enum class ApprovalStatus {
    UNAPPROVED, APPROVED, REJECTED
}