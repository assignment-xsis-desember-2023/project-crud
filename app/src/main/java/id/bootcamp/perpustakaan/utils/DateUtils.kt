package id.bootcamp.perpustakaan.utils

import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit
import java.util.Date
import java.util.Locale

fun formatDateUsingDefaultLocale(pattern: String, date: Date): String {
    return SimpleDateFormat(pattern, Locale.getDefault()).format(date)
}

fun formatDateFullWithShortMonth(date: Date): String {
    return formatDateUsingDefaultLocale("d MMM yyyy", date)
}

fun formatDateFullWithFullMonth(date: Date): String {
    return formatDateUsingDefaultLocale("d MMMM yyyy", date)
}

fun Date.plusDefaultBorrowDuration(): Date {
    return Date.from(this.toInstant().plus(7, ChronoUnit.DAYS))
}

fun Date.getMilliTruncatedToDays(): Long {
    return this.toInstant().truncatedTo(ChronoUnit.DAYS).toEpochMilli()
}