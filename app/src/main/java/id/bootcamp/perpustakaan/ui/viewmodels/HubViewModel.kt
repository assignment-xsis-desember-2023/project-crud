package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.data.repository.UserRepository
import id.bootcamp.perpustakaan.data.room.entity.UserEntity
import kotlinx.coroutines.launch

class HubViewModel(private val userRepository: UserRepository): ViewModel() {
    val userData = MutableLiveData<UserEntity>()

    fun getUserData(id: Int) = viewModelScope.launch {
        userData.postValue(userRepository.getUserById(id))
    }
}