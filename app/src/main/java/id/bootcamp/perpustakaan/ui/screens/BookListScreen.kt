package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.ui.components.BookImage
import id.bootcamp.perpustakaan.ui.components.LikeButton
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.BookListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory

@Composable
fun BookListScreen(
    bookListViewModel: BookListViewModel,
    currentUserId: Int,
    onBorrow: (BookModel) -> Unit
) {
    bookListViewModel.displayAllBooks(currentUserId)

    val bookListLiveData by bookListViewModel.bookList.observeAsState()

    val bookList = bookListLiveData

    if (bookList != null) {
        TopBarScaffold(title = "Daftar Buku") {
            LazyColumn(
                Modifier
                    .fillMaxSize()
                    .padding(it)
                    .padding(8.dp)
            ) {
                items(bookList) {
                    BookListItem(
                        book = it,
                        onBorrow = onBorrow,
                        onToggleLike = { book ->
                            bookListViewModel.toggleLikeBook(book, currentUserId)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun BookListItem(
    book: BookModel,
    onBorrow: (BookModel) -> Unit,
    onToggleLike: (BookModel) -> Unit
) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(4.dp, 8.dp)
    ) {
        BookImage(bookData = book)
        Column(Modifier.weight(1.0F)) {
            Text(
                text = book.name,
                modifier = Modifier.padding(8.dp)
            )
            if (book.isAvailable) {
                Button(
                    modifier = Modifier.padding(8.dp),
                    onClick = { onBorrow(book) }
                ) {
                    Text(text = "Pinjam")
                }
            } else {
                Text(
                    text = "Sedang tidak bisa dipinjam",
                    modifier = Modifier.padding(8.dp)
                )
            }
        }
        LikeButton(
            onToggleLike = { onToggleLike(book) },
            isLiked = book.isFavorite
        )
    }
}

@Preview
@Composable
fun BookListScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = BookListViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        BookListScreen(bookListViewModel = viewModel, currentUserId = 1, onBorrow = {})
    }
}