package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.ui.components.BookImage
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.FavoriteListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory

@Composable
fun FavoriteListScreen(
    viewModel: FavoriteListViewModel,
    userId: Int,
    onBorrow: (BookModel) -> Unit
) {
    viewModel.getFavoriteBooks(userId)

    val favoriteListLiveData by viewModel.favoriteList.observeAsState()

    // For smart cast purpose
    val favoriteList = favoriteListLiveData

    if (favoriteList != null) {
        TopBarScaffold(title = "Buku Favorit") { scaffoldPadding ->
            LazyColumn(
                Modifier
                    .fillMaxSize()
                    .padding(scaffoldPadding)
                    .padding(8.dp)
            ) {
                items(favoriteList) {
                    FavoriteListItem(
                        book = it,
                        onUnlike = { book ->
                            viewModel.toggleLikeBook(book.id, userId)
                        },
                        onBorrow = onBorrow
                    )
                }
            }
        }
    }
}

@Composable
fun FavoriteListItem(
    book: BookModel,
    onUnlike: (BookModel) -> Unit,
    onBorrow: (BookModel) -> Unit
) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(4.dp, 8.dp)
    ) {
        BookImage(bookData = book)
        Column {
            Text(
                text = book.name,
                style = MaterialTheme.typography.titleMedium,
                fontStyle = FontStyle.Italic,
                modifier = Modifier.padding(12.dp, 12.dp, 12.dp, 4.dp)
            )
            Row {
                if (book.isAvailable) {
                    Button(
                        modifier = Modifier.padding(8.dp),
                        onClick = { onBorrow(book) }
                    ) {
                        Text(text = "Pinjam")
                    }
                }
                Button(
                    modifier = Modifier.padding(8.dp),
                    onClick = { onUnlike(book) }
                ) {
                    Text(text = "Hapus dari Favorit")
                }
            }
        }
    }
}

@Preview
@Composable
fun FavoriteListScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = FavoriteListViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        FavoriteListScreen(viewModel = viewModel, userId = 1, onBorrow = {})
    }
}