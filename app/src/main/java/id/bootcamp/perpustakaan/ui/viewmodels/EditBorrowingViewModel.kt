package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.domain.usecase.BorrowBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.RequestDeadlineExtensionUseCase
import kotlinx.coroutines.launch
import java.util.Date

class EditBorrowingViewModel(
    private val getBookUseCase: GetBookUseCase,
    private val borrowBookUseCase: BorrowBookUseCase,
    private val requestDeadlineExtensionUseCase: RequestDeadlineExtensionUseCase,
    private val borrowingRepository: BorrowingRepository
): ViewModel() {
    val bookData = MutableLiveData<BookModel>()
    val borrowingData = MutableLiveData<BorrowingEntity>()

    fun getBookById(id: Int, isAvailable: Boolean) = viewModelScope.launch {
        bookData.postValue(getBookUseCase(id, isAvailable))
    }

    fun sendBorrowingRequest(
        bookData: BookModel,
        userId: Int,
        borrowingId: Int?,
        borrowDate: Date,
        returnDeadline: Date,
        isNewRequest: Boolean
    ) = viewModelScope.launch {
        borrowBookUseCase(bookData, userId, borrowingId, borrowDate, returnDeadline, isNewRequest)
    }

    fun sendReturnDeadlineExtensionRequest(
        borrowingId: Int,
        newReturnDeadline: Date
    ) = viewModelScope.launch {
        requestDeadlineExtensionUseCase(borrowingId, newReturnDeadline)
    }

    fun getBorrowingById(borrowingId: Int) = viewModelScope.launch {
        borrowingData.postValue(borrowingRepository.getBorrowingById(borrowingId))
    }
}