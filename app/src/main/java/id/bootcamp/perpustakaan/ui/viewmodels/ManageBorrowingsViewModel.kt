package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.room.entity.relation.BorrowingWithBookAndUser
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import kotlinx.coroutines.launch
import java.util.Date

class ManageBorrowingsViewModel(private val borrowingRepository: BorrowingRepository): ViewModel() {
    val borrowingList = MutableLiveData<List<BorrowingWithBookAndUser>>()

    fun displayAllBorrowings() = viewModelScope.launch {
        borrowingList.postValue(borrowingRepository.getAllBorrowings())
    }

    fun setApprovalStatus(borrowing: BorrowingWithBookAndUser, status: ApprovalStatus) = viewModelScope.launch {
        borrowingRepository.updateBorrowingApprovalStatus(borrowing, status)
    }

    fun respondDeadlineExtensionRequest(borrowing: BorrowingWithBookAndUser, isAccepted: Boolean) = viewModelScope.launch {
        borrowingRepository.updateBorrowingDeadlineExtension(borrowing, isAccepted)
    }

    fun confirmReturn(borrowing: BorrowingWithBookAndUser) = viewModelScope.launch {
        borrowingRepository.updateBorrowingReturnDate(borrowing, Date())
    }
}