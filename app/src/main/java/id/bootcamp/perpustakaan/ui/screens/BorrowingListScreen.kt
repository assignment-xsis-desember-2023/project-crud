package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.domain.model.BorrowingModel
import id.bootcamp.perpustakaan.ui.components.BookImage
import id.bootcamp.perpustakaan.ui.components.LikeButton
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.BorrowingListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import id.bootcamp.perpustakaan.utils.formatDateFullWithShortMonth

@Composable
fun BorrowingListScreen(viewModel: BorrowingListViewModel, userId: Int, onEditItem: (BorrowingModel) -> Unit) {
    viewModel.displayBorrowingByUserId(userId)

    val borrowingListLiveData by viewModel.borrowingList.observeAsState()

    val borrowingList = borrowingListLiveData

    if (borrowingList != null) {
        TopBarScaffold(title = "Daftar Pinjaman") {
            LazyColumn(
                Modifier
                    .fillMaxSize()
                    .padding(it)
                    .padding(8.dp)
            ) {
                groupedItems(
                    borrowingList = borrowingList,
                    status = ApprovalStatus.APPROVED,
                    onEditItem = onEditItem,
                    onToggleItemLike = { viewModel.toggleLikeBook(it.bookId, it.userId) }
                )
                groupedItems(
                    borrowingList = borrowingList,
                    status = ApprovalStatus.UNAPPROVED,
                    onEditItem = onEditItem,
                    onToggleItemLike = { viewModel.toggleLikeBook(it.bookId, it.userId) }
                )
                groupedItems(
                    borrowingList = borrowingList,
                    status = ApprovalStatus.REJECTED,
                    onEditItem = { /* Should be uneditable */},
                    onToggleItemLike = { viewModel.toggleLikeBook(it.bookId, it.userId) }
                )
            }
        }
    }
}

fun LazyListScope.groupedItems(
    borrowingList: List<BorrowingModel>,
    status: ApprovalStatus,
    onEditItem: (BorrowingModel) -> Unit,
    onToggleItemLike: (BorrowingModel) -> Unit
) {
    if (borrowingList.any { it.approvalStatus == status }) {
        item {
            Text(
                text = status.name.lowercase().replaceFirstChar { it.uppercaseChar() },
                style = MaterialTheme.typography.titleMedium
            )
        }
        items(borrowingList.filter { it.approvalStatus == status }) {
            BorrowingListItem(
                borrowingData = it,
                onEdit = onEditItem,
                onToggleLike = onToggleItemLike
            )
        }
    }
}

@Composable
fun BorrowingListItem(
    borrowingData: BorrowingModel,
    onEdit: (BorrowingModel) -> Unit,
    onToggleLike: (BorrowingModel) -> Unit
) {
    val bookData = BookModel(borrowingData.bookId, borrowingData.bookName, borrowingData.bookImage, false, borrowingData.isBookFavorite)
    Row {
        BookImage(bookData = bookData)
        Column(
            modifier = Modifier
                .weight(1.0F)
                .padding(4.dp, 8.dp)
        ) {
            Text(
                text = borrowingData.bookName,
                style = MaterialTheme.typography.titleMedium,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = "Dipinjam pada: ${formatDateFullWithShortMonth(borrowingData.borrowDate)}"
            )
            Text(
                text = "Batas pengembalian: ${formatDateFullWithShortMonth(borrowingData.returnDeadline)}"
            )
            val requestedNewBorrowingDeadline = borrowingData.returnDeadlineExtension
            if (requestedNewBorrowingDeadline != null) {
                Text(
                    text = "Diminta diubah ke: ${formatDateFullWithShortMonth(requestedNewBorrowingDeadline)}",
                    fontStyle = FontStyle.Italic
                )
            }
            if (borrowingData.approvalStatus == ApprovalStatus.APPROVED) {
                val returnDate = borrowingData.returnDate
                Text(
                    text = if (returnDate != null) {
                        "Dikembalikan pada: ${formatDateFullWithShortMonth(returnDate)}"
                    } else "Belum dikembalikan"
                )
            }
            if (borrowingData.isActive()) {
                Button(onClick = { onEdit(borrowingData) }) {
                    Text(text = if (borrowingData.approvalStatus == ApprovalStatus.APPROVED)
                        "Request Extension"
                    else
                        "Edit Request"
                    )
                }
            }
        }
        LikeButton(
            onToggleLike = { onToggleLike(borrowingData) },
            isLiked = borrowingData.isBookFavorite
        )
    }
}

@Preview
@Composable
fun BorrowingListScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = BorrowingListViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        BorrowingListScreen(viewModel = viewModel, userId = 1, onEditItem = {})
    }
}