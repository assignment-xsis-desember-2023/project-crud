package id.bootcamp.perpustakaan.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import id.bootcamp.perpustakaan.ui.screens.BookListScreen
import id.bootcamp.perpustakaan.ui.screens.BorrowingListScreen
import id.bootcamp.perpustakaan.ui.screens.EditBorrowingScreen
import id.bootcamp.perpustakaan.ui.screens.FavoriteListScreen
import id.bootcamp.perpustakaan.ui.screens.HubScreen
import id.bootcamp.perpustakaan.ui.screens.ManageBorrowingsScreen
import id.bootcamp.perpustakaan.ui.screens.SwitchUserScreen
import id.bootcamp.perpustakaan.ui.viewmodels.BookListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.BorrowingListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.EditBorrowingViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.FavoriteListViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.HubViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.ManageBorrowingsViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory
import id.bootcamp.perpustakaan.ui.viewmodels.SwitchUserViewModel

@Composable
fun MainNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "switch_user") {
        composable(
            "hub/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        ) {
            val userId = it.arguments?.getInt("userId")
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = HubViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            if (userId != null) {
                HubScreen(
                    viewModel = viewModel,
                    userId = userId,
                    onNavigateToBookList = {
                        navController.navigate("book_list/${userId}")
                    },
                    onNavigateToFavoriteList = {
                        navController.navigate("favorite_list/${userId}")
                    },
                    onNavigateToBorrowingList = {
                        navController.navigate("borrowing_list/${userId}")
                    },
                    onNavigateToManageBorrowings = {
                        navController.navigate("manage_borrowings/${userId}")
                    }
                )
            }
        }
        composable("switch_user") {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = SwitchUserViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            SwitchUserScreen(
                viewModel = viewModel,
                onConfirmChoice = {
                    navController.navigate("hub/${it.id}")
                }
            )
        }
        composable("book_list/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        ) {
            val userId = it.arguments?.getInt("userId") ?: 0
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = BookListViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            BookListScreen(
                bookListViewModel = viewModel,
                currentUserId = userId,
                onBorrow = { book ->
                    navController.navigate("edit_borrowing/${userId}/${book.id}")
                }
            )
        }
        composable("borrowing_list/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        ) {
            val userId = it.arguments?.getInt("userId") ?: 0
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = BorrowingListViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            BorrowingListScreen(
                viewModel = viewModel,
                userId = userId,
                onEditItem = { borrowingData ->
                    navController.navigate("edit_borrowing/${userId}/${borrowingData.bookId}?editedBorrowingId=${borrowingData.id}")
                }
            )
        }
        composable("favorite_list/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        ) {
            val userId = it.arguments?.getInt("userId") ?: 0
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = FavoriteListViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            FavoriteListScreen(
                viewModel = viewModel,
                userId = userId,
                onBorrow = { book ->
                    navController.navigate("edit_borrowing/${userId}/${book.id}")
                }
            )
        }
        composable("manage_borrowings/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        ) {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = ManageBorrowingsViewModel::class.java,
                factory = PerpustakaanViewModelFactory.getInstance(context)
            )
            ManageBorrowingsScreen(
                viewModel = viewModel
            )
        }
        composable("edit_borrowing/{userId}/{bookId}?editedBorrowingId={editedBorrowingId}",
            arguments = listOf(
                navArgument("userId") { type = NavType.IntType },
                navArgument("bookId") { type = NavType.IntType },
                navArgument("editedBorrowingId") { nullable = true }
            )
        ) {
            val userId = it.arguments?.getInt("userId")
            val bookId = it.arguments?.getInt("bookId")
            val editedBorrowingId = it.arguments?.getString("editedBorrowingId")?.toInt()

            if (userId == null || bookId == null) {
                navController.popBackStack()
            } else {
                val context = LocalContext.current
                val viewModel = viewModel(
                    modelClass = EditBorrowingViewModel::class.java,
                    factory = PerpustakaanViewModelFactory.getInstance(context)
                )
                if (editedBorrowingId == null) {
                    EditBorrowingScreen(
                        viewModel = viewModel,
                        userId = userId,
                        bookId = bookId,
                        onFinish = {
                            navController.popBackStack()
                        }
                    )
                } else {
                    EditBorrowingScreen(
                        viewModel = viewModel,
                        userId = userId,
                        bookId = bookId,
                        borrowingId = editedBorrowingId,
                        onFinish = {
                            navController.popBackStack()
                        }
                    )
                }
            }
        }
    }
}