package id.bootcamp.perpustakaan.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import id.bootcamp.perpustakaan.domain.model.BookModel

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun BookImage(bookData: BookModel, modifier: Modifier = Modifier) {
    GlideImage(
        model = bookData.image,
        contentDescription = bookData.name,
        contentScale = ContentScale.Fit,
        modifier = modifier
            .size(75.dp, 100.dp)
            .padding(8.dp)
            .border(1.dp, MaterialTheme.colorScheme.primary, CardDefaults.shape)
            .background(MaterialTheme.colorScheme.onSurface, CardDefaults.shape)
            .clip(CardDefaults.shape)
    )
}