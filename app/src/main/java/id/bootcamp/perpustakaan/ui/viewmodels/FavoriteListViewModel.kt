package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.domain.usecase.GetBookListUseCase
import id.bootcamp.perpustakaan.domain.usecase.ToggleLikeBookUseCase
import kotlinx.coroutines.launch

class FavoriteListViewModel(
    private val getBookListUseCase: GetBookListUseCase,
    private val toggleLikeBookUseCase: ToggleLikeBookUseCase
): ViewModel() {
    val favoriteList = MutableLiveData<List<BookModel>>()

    fun getFavoriteBooks(userId: Int) = viewModelScope.launch {
        favoriteList.postValue(getBookListUseCase(userId).filter { it.isFavorite })
    }

    fun toggleLikeBook(bookId: Int, userId: Int) = viewModelScope.launch {
        toggleLikeBookUseCase(bookId, userId)
    }
}