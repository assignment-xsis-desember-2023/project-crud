package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SelectableDates
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.EditBorrowingViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import id.bootcamp.perpustakaan.utils.formatDateFullWithFullMonth
import id.bootcamp.perpustakaan.utils.getMilliTruncatedToDays
import id.bootcamp.perpustakaan.utils.plusDefaultBorrowDuration
import java.time.temporal.ChronoUnit
import java.util.Date

@Composable
fun EditBorrowingScreen(
    viewModel: EditBorrowingViewModel,
    userId: Int,
    bookId: Int,
    borrowingId: Int,
    onFinish: () -> Unit
) {
    viewModel.getBorrowingById(borrowingId)

    val borrowingLiveData by viewModel.borrowingData.observeAsState()

    // For smart cast purpose
    val borrowingData = borrowingLiveData

    if (borrowingData != null) {
        EditBorrowingScreen(
            viewModel = viewModel,
            userId = userId,
            bookId = bookId,
            borrowingId = borrowingId,
            onFinish = onFinish,
            isNewRequest = false,
            approvalStatus = borrowingData.approvalStatus,
            initialBorrowDate = borrowingData.borrowDate,
            initialReturnDeadline = borrowingData.returnDeadline
        )
    }
}

@Composable
fun EditBorrowingScreen(
    viewModel: EditBorrowingViewModel,
    userId: Int,
    bookId: Int,
    onFinish: () -> Unit,
    isNewRequest: Boolean = true,
    initialBorrowDate: Date = Date(),
    initialReturnDeadline: Date = Date().plusDefaultBorrowDuration(),
    borrowingId: Int? = null,
    approvalStatus: ApprovalStatus? = null
) {
    viewModel.getBookById(bookId, isNewRequest)

    val bookLiveData by viewModel.bookData.observeAsState()

    // For smart cast purpose
    val bookData = bookLiveData

    var chosenStartDate by rememberSaveable {
        mutableStateOf(initialBorrowDate)
    }
    var chosenEndDate by rememberSaveable {
        mutableStateOf(initialReturnDeadline)
    }

    if (bookData != null) {
        TopBarScaffold(title = if (isNewRequest) "Borrow Book" else "Edit Borrowing Request") {
            LazyColumn(
                Modifier
                    .padding(it)
                    .padding(16.dp, 8.dp)
            ) {
                item {
                    EditBorrowingHeader(text = "Nama Buku:")
                }
                item {
                    Text(
                        bookData.name
                    )
                }
                item {
                    EditBorrowingHeader(text = "Tanggal Mulai Meminjam:")
                }
                item {
                    BorrowingDateInput(
                        chosenDate = chosenStartDate,
                        inclusiveSelectableStart = Date(),
                        onInput = { chosenDate -> chosenStartDate = chosenDate },
                        enabled = (approvalStatus != ApprovalStatus.APPROVED)
                    )
                }
                item {
                    EditBorrowingHeader(text = "Tanggal Pengembalian:")
                }
                item {
                    BorrowingDateInput(
                        chosenDate = chosenEndDate,
                        inclusiveSelectableStart = maxOf(chosenStartDate, Date()),
                        onInput = { chosenDate -> chosenEndDate = chosenDate }
                    )
                }
                item {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Button(
                            modifier = Modifier
                                .padding(16.dp),
                            onClick = {
                                if (approvalStatus == ApprovalStatus.APPROVED) {
                                    viewModel.sendReturnDeadlineExtensionRequest(
                                        borrowingId!!,
                                        chosenEndDate
                                    )
                                } else {
                                    viewModel.sendBorrowingRequest(
                                        bookData,
                                        userId,
                                        borrowingId,
                                        chosenStartDate,
                                        chosenEndDate,
                                        isNewRequest
                                    )
                                }
                                onFinish()
                            }
                        ) {
                            Text(text = if (isNewRequest) "Kirim Permintaan Pinjaman" else "Perbarui Permintaan Pinjaman")
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun EditBorrowingHeader(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleMedium,
        modifier = modifier.padding(0.dp, 16.dp, 0.dp, 0.dp)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BorrowingDateInput(
    chosenDate: Date,
    inclusiveSelectableStart: Date,
    onInput: (Date) -> Unit,
    enabled: Boolean = true
) {
    var showDialog by rememberSaveable { mutableStateOf(false) }
    Text(
        text = formatDateFullWithFullMonth(chosenDate),
        modifier = (if (enabled) Modifier
            .clickable { showDialog = true }
        else Modifier
            .background(MaterialTheme.colorScheme.surfaceVariant,  RoundedCornerShape(4.dp))
        )
            .border(1.dp, MaterialTheme.colorScheme.primary, RoundedCornerShape(4.dp))
            .padding(8.dp)
            .width(200.dp)
    )
    if (showDialog) {
        val datePickerState = rememberDatePickerState(
            initialSelectedDateMillis = chosenDate.time,
            selectableDates = object: SelectableDates {
                override fun isSelectableDate(utcTimeMillis: Long): Boolean {
                    return utcTimeMillis >= inclusiveSelectableStart.getMilliTruncatedToDays()
                }
            }
        )
        DatePickerDialog(
            onDismissRequest = { showDialog = false },
            confirmButton = {
                Button(
                    onClick = {
                        val selectedDateMilis = datePickerState.selectedDateMillis
                        if (selectedDateMilis != null) {
                            onInput(Date(selectedDateMilis))
                        }
                        showDialog = false
                    }
                ) {
                    Text(text = "OK")
                }
            }
        ) {
            DatePicker(
                state = datePickerState
            )
        }
    }
}

@Preview
@Composable
fun EditBorrowingScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = EditBorrowingViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        EditBorrowingScreen(
            viewModel = viewModel,
            userId = 1,
            bookId = 1,
            onFinish = {},
            isNewRequest = false,
            initialBorrowDate = Date(Date().toInstant().plus(7, ChronoUnit.DAYS).toEpochMilli()),
            initialReturnDeadline = Date(Date().toInstant().plus(14, ChronoUnit.DAYS).toEpochMilli())
        )
    }
}