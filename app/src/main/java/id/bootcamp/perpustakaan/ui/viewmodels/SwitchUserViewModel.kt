package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.data.repository.UserRepository
import id.bootcamp.perpustakaan.data.room.entity.UserEntity
import kotlinx.coroutines.launch

class SwitchUserViewModel(private val userRepository: UserRepository): ViewModel() {
    val userList = MutableLiveData<List<UserEntity>>()
    val chosenUser = MutableLiveData<UserEntity>()

    fun displayAllUser() = viewModelScope.launch {
        userList.postValue(userRepository.getAllUsers())
    }

    fun chooseUser(userEntity: UserEntity) {
        chosenUser.postValue(userEntity)
    }
}