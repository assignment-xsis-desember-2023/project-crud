package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.domain.model.BorrowingModel
import id.bootcamp.perpustakaan.domain.usecase.GetBorrowingListByUserUseCase
import id.bootcamp.perpustakaan.domain.usecase.ToggleLikeBookUseCase
import kotlinx.coroutines.launch

class BorrowingListViewModel(
    private val getBorrowingListByUserUseCase: GetBorrowingListByUserUseCase,
    private val toggleLikeBookUseCase: ToggleLikeBookUseCase
): ViewModel() {
    val borrowingList = MutableLiveData<List<BorrowingModel>>()

    fun displayBorrowingByUserId(userId: Int) = viewModelScope.launch {
        borrowingList.postValue(getBorrowingListByUserUseCase(userId))
    }

    fun toggleLikeBook(bookId: Int, userId: Int) = viewModelScope.launch {
        toggleLikeBookUseCase(bookId, userId)
    }
}