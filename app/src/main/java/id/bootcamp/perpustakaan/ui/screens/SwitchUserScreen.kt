package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.data.room.entity.UserEntity
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory
import id.bootcamp.perpustakaan.ui.viewmodels.SwitchUserViewModel

@Composable
fun SwitchUserScreen(viewModel: SwitchUserViewModel, onConfirmChoice: (UserEntity) -> Unit) {
    viewModel.displayAllUser()
    
    val userListLiveData by viewModel.userList.observeAsState()
    val chosenUserLiveData by viewModel.chosenUser.observeAsState()
    
    // For smart cast purpose
    val userList = userListLiveData
    val chosenUser = chosenUserLiveData
    
    if (userList != null) {
        TopBarScaffold(title = "Pilih User") { scaffoldPadding ->
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(scaffoldPadding)
            ) {
                UserListDropdownMenu(
                    userList = userList,
                    currentChoice = chosenUser?.name,
                    onChoose = {
                        viewModel.chooseUser(it)
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                Button(
                    onClick = {
                        if (chosenUser != null) {
                            onConfirmChoice(chosenUser)
                        }
                    }
                ) {
                    Text(text = "Masuk")
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserListDropdownMenu(
    userList: List<UserEntity>,
    currentChoice: String?,
    onChoose: (UserEntity) -> Unit,
    modifier: Modifier = Modifier
) {
    var isExpanded by remember { mutableStateOf(false) }
    ExposedDropdownMenuBox(
        expanded = isExpanded,
        onExpandedChange = { isExpanded = !isExpanded },
        modifier = modifier
    ) {
        OutlinedTextField(
            value = currentChoice ?: "",
            onValueChange = {},
            readOnly = true,
            label = {
                Text(text = "Nama Pengguna")
            },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = isExpanded)
            },
            modifier = Modifier.menuAnchor()
        )
        ExposedDropdownMenu(
            expanded = isExpanded,
            onDismissRequest = { isExpanded = false }
        ) {
            for (user in userList) {
                DropdownMenuItem(
                    text = {
                        Text(user.name)
                    },
                    onClick = {
                        isExpanded = false
                        onChoose(user)
                    }
                )
            }
        }
    }
}

@Preview
@Composable
fun SwitchUserScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = SwitchUserViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        SwitchUserScreen(viewModel = viewModel, onConfirmChoice = {})
    }
}