package id.bootcamp.perpustakaan.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable

@Composable
fun LikeButton(onToggleLike: () -> Unit, isLiked: Boolean) {
    IconButton(onClick = onToggleLike) {
        if (isLiked) {
            Icon(imageVector = Icons.Default.Favorite, contentDescription = "Like")
        } else {
            Icon(imageVector = Icons.Default.FavoriteBorder, contentDescription = "Unlike")
        }
    }
}