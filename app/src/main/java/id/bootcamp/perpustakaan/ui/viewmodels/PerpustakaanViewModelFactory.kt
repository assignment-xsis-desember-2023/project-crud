package id.bootcamp.perpustakaan.ui.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.repository.UserRepository
import id.bootcamp.perpustakaan.domain.usecase.BorrowBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBookListUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBookUseCase
import id.bootcamp.perpustakaan.domain.usecase.GetBorrowingListByUserUseCase
import id.bootcamp.perpustakaan.domain.usecase.RequestDeadlineExtensionUseCase
import id.bootcamp.perpustakaan.domain.usecase.ToggleLikeBookUseCase
import id.bootcamp.perpustakaan.injection.Injection

class PerpustakaanViewModelFactory(
    private val borrowingRepository: BorrowingRepository,
    private val userRepository: UserRepository,
    private val getBookListUseCase: GetBookListUseCase,
    private val borrowBookUseCase: BorrowBookUseCase,
    private val getBookUseCase: GetBookUseCase,
    private val requestDeadlineExtensionUseCase: RequestDeadlineExtensionUseCase,
    private val toggleLikeBookUseCase: ToggleLikeBookUseCase,
    private val getBorrowingListByUserUseCase: GetBorrowingListByUserUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BookListViewModel::class.java)) {
            return BookListViewModel(getBookListUseCase, toggleLikeBookUseCase) as T
        } else if (modelClass.isAssignableFrom(BorrowingListViewModel::class.java)) {
            return BorrowingListViewModel(getBorrowingListByUserUseCase, toggleLikeBookUseCase) as T
        } else if (modelClass.isAssignableFrom(SwitchUserViewModel::class.java)) {
            return SwitchUserViewModel(userRepository) as T
        } else if (modelClass.isAssignableFrom(ManageBorrowingsViewModel::class.java)) {
            return ManageBorrowingsViewModel(borrowingRepository) as T
        } else if (modelClass.isAssignableFrom(EditBorrowingViewModel::class.java)) {
            return EditBorrowingViewModel(getBookUseCase, borrowBookUseCase, requestDeadlineExtensionUseCase, borrowingRepository) as T
        } else if (modelClass.isAssignableFrom(FavoriteListViewModel::class.java)) {
            return FavoriteListViewModel(getBookListUseCase, toggleLikeBookUseCase) as T
        } else if (modelClass.isAssignableFrom(HubViewModel::class.java)) {
            return HubViewModel(userRepository) as T
        }
        return super.create(modelClass)
    }

    companion object {
        @Volatile
        private var instance: PerpustakaanViewModelFactory? = null
        fun getInstance(context: Context): PerpustakaanViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: PerpustakaanViewModelFactory(
                    Injection.provideBorrowingRepository(context),
                    Injection.provideUserRepository(context),
                    Injection.provideGetBookListUseCase(context),
                    Injection.provideBorrowBookUseCase(context),
                    Injection.provideGetBookUseCase(context),
                    Injection.provideRequestDeadlineExtensionUseCase(context),
                    Injection.provideToggleLikeBookUseCase(context),
                    Injection.provideGetBorrowingListByUserUseCase(context)
                )
            }.also { instance = it }
    }
}