package id.bootcamp.perpustakaan.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.domain.usecase.GetBookListUseCase
import id.bootcamp.perpustakaan.domain.usecase.ToggleLikeBookUseCase
import kotlinx.coroutines.launch

class BookListViewModel(
    private val getBookListUseCase: GetBookListUseCase,
    private val toggleLikeBookUseCase: ToggleLikeBookUseCase
): ViewModel() {
    val bookList = MutableLiveData<List<BookModel>>()

    fun displayAllBooks(currentUserId: Int) = viewModelScope.launch {
        bookList.postValue(getBookListUseCase(currentUserId))
    }

    fun toggleLikeBook(book: BookModel, userId: Int) = viewModelScope.launch {
        toggleLikeBookUseCase(book, userId)
    }
}