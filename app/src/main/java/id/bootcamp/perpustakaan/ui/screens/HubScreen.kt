package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.HubViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory

@Composable
fun HubScreen(
    viewModel: HubViewModel,
    userId: Int,
    onNavigateToBookList: () -> Unit,
    onNavigateToFavoriteList: () -> Unit,
    onNavigateToBorrowingList: () -> Unit,
    onNavigateToManageBorrowings: () -> Unit,
) {
    viewModel.getUserData(userId)

    val userLiveData by viewModel.userData.observeAsState()

    // For smart cast purpose
    val userData = userLiveData

    if (userData != null) {
        TopBarScaffold(title = "Perpustakaan") {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.padding(it).fillMaxSize()
            ) {
                HubHeaderText(
                    text = "Selamat Datang, ${userData.name}!",
                    modifier = Modifier.padding(16.dp)
                )
                HubHeaderText(
                    text = "Ayo Lihat-Lihat!"
                )
                Button(onClick = onNavigateToBookList) {
                    Text("Lihat Daftar Buku Lengkap")
                }
                Button(onClick = onNavigateToFavoriteList) {
                    Text("Lihat Daftar Buku Favorit")
                }
                Button(onClick = onNavigateToBorrowingList) {
                    Text("Lihat Daftar Pinjaman")
                }
                if (userData.userGroup == "admin") {
                    HubHeaderText(
                        text = "Perkakas Admin"
                    )
                    Button(onClick = onNavigateToManageBorrowings) {
                        Text("Kelola Pinjaman")
                    }
                }
            }
        }
    }
}

@Composable
fun HubHeaderText(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleLarge,
        modifier = modifier.padding(0.dp, 24.dp, 0.dp, 8.dp)
    )
}

@Preview
@Composable
fun HubScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = HubViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        HubScreen(
            viewModel = viewModel,
            userId = 1,
            onNavigateToBookList = { },
            onNavigateToFavoriteList = { },
            onNavigateToBorrowingList = { },
            onNavigateToManageBorrowings = { }
        )
    }
}