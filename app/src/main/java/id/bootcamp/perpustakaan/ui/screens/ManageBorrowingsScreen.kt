package id.bootcamp.perpustakaan.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.perpustakaan.data.room.entity.relation.BorrowingWithBookAndUser
import id.bootcamp.perpustakaan.domain.mapper.toDomainModel
import id.bootcamp.perpustakaan.ui.components.BookImage
import id.bootcamp.perpustakaan.ui.components.TopBarScaffold
import id.bootcamp.perpustakaan.ui.theme.MyApplicationTheme
import id.bootcamp.perpustakaan.ui.viewmodels.ManageBorrowingsViewModel
import id.bootcamp.perpustakaan.ui.viewmodels.PerpustakaanViewModelFactory
import id.bootcamp.perpustakaan.utils.ApprovalStatus
import id.bootcamp.perpustakaan.utils.formatDateFullWithShortMonth
import java.util.Date

@Composable
fun ManageBorrowingsScreen(viewModel: ManageBorrowingsViewModel) {
    viewModel.displayAllBorrowings()

    val borrowingListLiveData by viewModel.borrowingList.observeAsState()

    // For smart cast purpose
    val borrowingList = borrowingListLiveData

    if (borrowingList != null) {
        TopBarScaffold(title = "Kelola Pinjaman") { scaffoldPadding ->
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(scaffoldPadding)
                    .padding(8.dp)
            ) {
                groupedItemsManage(
                    borrowingList = borrowingList,
                    status = { it.borrowing.returnDate == null
                            && it.borrowing.returnDeadlineExtension == null },
                    statusName = "Unreturned",
                    onAccept = { viewModel.confirmReturn(it) },
                    onReject = { /* No button that applies to this function*/ }
                )
                groupedItemsManage(
                    borrowingList = borrowingList,
                    status = { it.borrowing.returnDate == null
                            && it.borrowing.returnDeadlineExtension != null },
                    statusName = "Edited",
                    onAccept = { viewModel.respondDeadlineExtensionRequest(it, true) },
                    onReject = { viewModel.respondDeadlineExtensionRequest(it, false) }
                )
                groupedItemsManage(
                    borrowingList = borrowingList,
                    status = ApprovalStatus.UNAPPROVED,
                    onAccept = { viewModel.setApprovalStatus(it, ApprovalStatus.APPROVED) },
                    onReject = { viewModel.setApprovalStatus(it, ApprovalStatus.REJECTED) }
                )
            }
        }
    }
}

fun LazyListScope.groupedItemsManage(
    borrowingList: List<BorrowingWithBookAndUser>,
    status: ApprovalStatus,
    onAccept: (BorrowingWithBookAndUser) -> Unit,
    onReject: (BorrowingWithBookAndUser) -> Unit
) {
    if (borrowingList.any { it.borrowing.approvalStatus == status }) {
        item {
            Text(
                text = status.name.lowercase().replaceFirstChar { it.uppercaseChar() },
                style = MaterialTheme.typography.titleMedium
            )
        }
        items(borrowingList.filter { it.borrowing.approvalStatus == status }) {
            ManageBorrowingsItem(
                borrowingData = it,
                onAccept = onAccept,
                onReject = onReject
            )
        }
    }
}

fun LazyListScope.groupedItemsManage(
    borrowingList: List<BorrowingWithBookAndUser>,
    status: (BorrowingWithBookAndUser) -> Boolean,
    statusName: String,
    onAccept: (BorrowingWithBookAndUser) -> Unit,
    onReject: (BorrowingWithBookAndUser) -> Unit
) {
    if (borrowingList.any { status(it) && it.borrowing.approvalStatus == ApprovalStatus.APPROVED }) {
        item {
            Text(
                text = statusName,
                style = MaterialTheme.typography.titleMedium
            )
        }
        items(borrowingList.filter { status(it) && it.borrowing.approvalStatus == ApprovalStatus.APPROVED }) {
            ManageBorrowingsItem(
                borrowingData = it,
                onAccept = onAccept,
                onReject = onReject
            )
        }
    }
}

@Composable
fun ManageBorrowingsItem(
    borrowingData: BorrowingWithBookAndUser,
    onAccept: (BorrowingWithBookAndUser) -> Unit,
    onReject: (BorrowingWithBookAndUser) -> Unit
) {
    Row {
        // Set isAvailable and isFavorite to arbitrary Boolean value as they are not relevant
        BookImage(bookData = borrowingData.book.toDomainModel(false, false))
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(4.dp, 8.dp)
        ) {
            Text(
                text = borrowingData.book.name,
                style = MaterialTheme.typography.titleMedium,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = "Peminta pinjaman: ${borrowingData.user.name}"
            )
            Text(
                text = "Dipinjam pada: ${formatDateFullWithShortMonth(borrowingData.borrowing.borrowDate)}"
            )
            Text(
                text = "Batas pengembalian: ${formatDateFullWithShortMonth(borrowingData.borrowing.returnDeadline)}"
            )
            if (borrowingData.borrowing.approvalStatus == ApprovalStatus.UNAPPROVED) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Button(onClick = { onAccept(borrowingData) }) {
                        Text("Approve")
                    }
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.error
                        ),
                        onClick = { onReject(borrowingData) }
                    ) {
                        Text("Reject")
                    }
                }
            } else if (borrowingData.borrowing.returnDate == null) {
                val requestedNewBorrowingDeadline = borrowingData.borrowing.returnDeadlineExtension
                if (requestedNewBorrowingDeadline == null) {
                    if (Date() >= borrowingData.borrowing.borrowDate) {
                        Button(onClick = { onAccept(borrowingData) }) {
                            Text("Confirm Return")
                        }
                    }
                } else {
                    Text(text = "Batas pengembalian baru: ${formatDateFullWithShortMonth(requestedNewBorrowingDeadline)}")
                    Row {
                        Button(onClick = { onAccept(borrowingData) }) {
                            Text("Approve")
                        }
                        Button(
                            colors = ButtonDefaults.buttonColors(
                                containerColor = MaterialTheme.colorScheme.error
                            ),
                            onClick = { onReject(borrowingData) }
                        ) {
                            Text("Reject")
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun ManageBorrowingsScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = ManageBorrowingsViewModel::class.java,
        factory = PerpustakaanViewModelFactory.getInstance(context)
    )
    MyApplicationTheme {
        ManageBorrowingsScreen(viewModel = viewModel)
    }
}