package id.bootcamp.perpustakaan.domain.mapper

import id.bootcamp.perpustakaan.data.room.entity.BookEntity
import id.bootcamp.perpustakaan.domain.model.BookModel

fun BookEntity.toDomainModel(isAvailable: Boolean, isFavorite: Boolean): BookModel {
    return BookModel(
        this.id,
        this.name,
        this.image,
        isAvailable,
        isFavorite
    )
}

fun BookModel.toEntity(): BookEntity {
    return BookEntity(
        this.id,
        this.name,
        this.image
    )
}