package id.bootcamp.perpustakaan.domain.model

class BookModel(
    val id: Int,
    val name: String,
    val image: String,
    val isAvailable: Boolean,
    val isFavorite: Boolean
) {
}