package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.BookRepository
import id.bootcamp.perpustakaan.data.repository.FavoriteRepository
import id.bootcamp.perpustakaan.domain.mapper.toDomainModel
import id.bootcamp.perpustakaan.domain.model.BookModel

class GetBookUseCase(
    private val bookRepository: BookRepository,
    private val favoriteRepository: FavoriteRepository
) {
    suspend operator fun invoke(bookId: Int, isAvailable: Boolean): BookModel {
        return bookRepository.getBookById(bookId).toDomainModel(isAvailable, false /*temp*/)
    }
}