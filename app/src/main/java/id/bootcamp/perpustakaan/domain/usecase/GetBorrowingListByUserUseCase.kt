package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.repository.FavoriteRepository
import id.bootcamp.perpustakaan.domain.mapper.toDomainModel
import id.bootcamp.perpustakaan.domain.model.BorrowingModel

class GetBorrowingListByUserUseCase(
    private val borrowingRepository: BorrowingRepository,
    private val favoriteRepository: FavoriteRepository
) {
    suspend operator fun invoke(userId: Int): List<BorrowingModel> {
        return borrowingRepository.getBorrowingsByUser(userId).map {
            it.toDomainModel(favoriteRepository.isLiked(it.book.id, it.user.id))
        }
    }
}