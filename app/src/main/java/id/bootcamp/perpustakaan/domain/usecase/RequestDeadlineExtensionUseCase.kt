package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import java.util.Date

class RequestDeadlineExtensionUseCase(
    private val borrowingRepository: BorrowingRepository
) {
    suspend operator fun invoke(
        borrowingId: Int,
        newReturnDeadline: Date
    ) {
        val borrowingData = borrowingRepository.getBorrowingById(borrowingId)
        val newBorrowingData = borrowingData.copy(returnDeadlineExtension = newReturnDeadline)
        borrowingRepository.updateBorrowing(newBorrowingData)
    }
}