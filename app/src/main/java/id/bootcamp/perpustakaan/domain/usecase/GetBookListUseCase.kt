package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.BookRepository
import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.repository.FavoriteRepository
import id.bootcamp.perpustakaan.domain.mapper.toDomainModel
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.utils.ApprovalStatus

class GetBookListUseCase(
    private val bookRepository: BookRepository,
    private val borrowingRepository: BorrowingRepository,
    private val favoriteRepository: FavoriteRepository
) {
    suspend operator fun invoke(userId: Int): List<BookModel> {
        val activeBorrowingList = borrowingRepository.getAllBorrowings()
            .filter { (it.borrowing.approvalStatus == ApprovalStatus.APPROVED
                    && it.borrowing.returnDate == null)
                    || it.borrowing.approvalStatus == ApprovalStatus.UNAPPROVED }
        val bookMapToBorrowing = bookRepository.getAllBooks()
            .associateWith { book -> activeBorrowingList.filter { it.book.id == book.id } }
        return bookMapToBorrowing.map { it.key.toDomainModel(it.value.isEmpty(), favoriteRepository.isLiked(it.key.id, userId)) }
    }
}