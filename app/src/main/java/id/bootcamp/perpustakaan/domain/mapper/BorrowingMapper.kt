package id.bootcamp.perpustakaan.domain.mapper

import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.data.room.entity.relation.BorrowingWithBookAndUser
import id.bootcamp.perpustakaan.domain.model.BorrowingModel

fun BorrowingWithBookAndUser.toDomainModel(isFavorite: Boolean): BorrowingModel {
    return BorrowingModel(
        id = this.borrowing.id,
        bookId = this.book.id,
        bookName = this.book.name,
        bookImage = this.book.image,
        isBookFavorite = isFavorite,
        userId = this.user.id,
        userName = this.user.name,
        borrowDate = this.borrowing.borrowDate,
        returnDeadline = this.borrowing.returnDeadline,
        returnDate = this.borrowing.returnDate,
        approvalStatus = this.borrowing.approvalStatus,
        returnDeadlineExtension = this.borrowing.returnDeadlineExtension
    )
}

fun BorrowingModel.toEntity(): BorrowingEntity {
    return BorrowingEntity(
        id = this.id,
        bookId = this.bookId,
        userId = this.userId,
        borrowDate = this.borrowDate,
        returnDeadline = this.returnDeadline,
        returnDate = this.returnDate,
        approvalStatus = this.approvalStatus
    )
}