package id.bootcamp.perpustakaan.domain.model

import id.bootcamp.perpustakaan.utils.ApprovalStatus
import java.util.Date

class BorrowingModel(
    val id: Int = 0,
    val bookId: Int,
    val bookName: String,
    val bookImage: String,
    val isBookFavorite: Boolean,
    val userId: Int,
    val userName: String,
    val borrowDate: Date,
    val returnDeadline: Date,
    val returnDate: Date? = null,
    var approvalStatus: ApprovalStatus = ApprovalStatus.UNAPPROVED,
    val returnDeadlineExtension: Date? = null
) {
    fun isActive(): Boolean {
        val isApprovedAndNotReturned =
            (approvalStatus == ApprovalStatus.APPROVED && returnDate == null)
        val isUnapproved = approvalStatus == ApprovalStatus.UNAPPROVED
        return isApprovedAndNotReturned || isUnapproved
    }
}