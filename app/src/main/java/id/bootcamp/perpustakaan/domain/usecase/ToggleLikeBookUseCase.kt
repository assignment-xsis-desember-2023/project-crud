package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.FavoriteRepository
import id.bootcamp.perpustakaan.data.room.entity.FavoriteEntity
import id.bootcamp.perpustakaan.domain.model.BookModel

class ToggleLikeBookUseCase(private val favoriteRepository: FavoriteRepository) {
    suspend operator fun invoke(book: BookModel, userId: Int) {
        this(FavoriteEntity(book.id, userId))
    }

    suspend operator fun invoke(bookId: Int, userId: Int) {
        this(FavoriteEntity(bookId, userId))
    }

    suspend operator fun invoke(favoriteEntity: FavoriteEntity) {
        val isFavorite = favoriteRepository.findEntity(favoriteEntity)
        if (!isFavorite) {
            favoriteRepository.addFavorite(favoriteEntity)
        } else {
            favoriteRepository.deleteFavorite(favoriteEntity)
        }
    }
}