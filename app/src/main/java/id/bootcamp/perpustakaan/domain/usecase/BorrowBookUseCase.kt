package id.bootcamp.perpustakaan.domain.usecase

import id.bootcamp.perpustakaan.data.repository.BorrowingRepository
import id.bootcamp.perpustakaan.data.room.entity.BorrowingEntity
import id.bootcamp.perpustakaan.domain.model.BookModel
import id.bootcamp.perpustakaan.utils.plusDefaultBorrowDuration
import java.util.Date

class BorrowBookUseCase(
    private val borrowingRepository: BorrowingRepository
) {
    suspend operator fun invoke(
        book: BookModel,
        userId: Int,
        borrowingId: Int? = null,
        borrowDate: Date = Date(),
        returnDeadline: Date = Date().plusDefaultBorrowDuration(),
        isNewRequest: Boolean = true
    ) {
        val borrowingEntity = BorrowingEntity(
            id = borrowingId ?: 0,
            bookId = book.id,
            userId = userId,
            borrowDate = borrowDate,
            returnDeadline = returnDeadline
        )
        if (isNewRequest) {
            borrowingRepository.addNewBorrowing(borrowingEntity)
        } else {
            borrowingRepository.updateBorrowing(borrowingEntity)
        }
    }
}